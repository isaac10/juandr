
function viewMas1(){
    var titulo = 'Cytogel ';
    var cuerpo = '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<p>'+
                            'Cytogel® matriz molecular regenerativa, es un novedoso avance de la tecnología en el proceso de reparación o renovación de los'+ 'tejidos. Es una matriz multimolecular regenerativa que actúa sobre células madre del paciente, proporcionando una carga importante'+ 'de factores de crecimiento y de otros elementos del mismo paciente (carga autóloga) que son indispensables para la regeneración de'+ 'los tejidos afectados por enfermedad, trauma o envejecimiento.'+
                            '¿Para qué sirve Cytogel?'+
                            'Porque además de su comprobada eficacia en múltiples procedimientos médicos y odontológicos, está exento de sustancias extrañas que'+ 'pueden provocar reacciones alérgicas o rechazos en quien lo recibe, dada su total compatibilidad, ya que proviene de la propia'+
                            'sangre del paciente'+
                            'Asegura una concentración óptima de plaquetas, para aportar una cantidad efectiva de factores de crecimiento que inducen a la'+ 'regeneración de tejidos.'+
                            'El proceso de obtención de las muestras y producción del biomaterial, se ejecuta bajo los más estrictos controles de asepsia y'+ 'antisepsia, lo que evita cualquier riesgo de contaminación o transmisión de infecciones.'+
                        '</p>'+
                    '</div>'+
                '</div>';
    var pie = '<button type="button" class="btn btn-default">Nice Button</button>'+
              '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>';
    startModal(titulo, cuerpo, pie);
}


function viewMdCarboxiterapia(){
    var titulo = 'Carboxiterapia ';
    var cuerpo = '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<p>'+
                            'El CO2, además de ser uno de los principales componentes de nuestra atmósfera es un activo con infinitas aplicaciones en el mundo'+ 'de la cosmética y con unos resultados de lo más sorprendentes.  La carboxiterapia tiene infinitas aplicaciones para mejorar la'+ 'belleza y salud de nuestra piel. Por ello, en los últimos años son muchos los centros de belleza que han incorporado este'+ 'tratamiento a su oferta, obteniendo unos resultados de lo más satisfactorios.'+

                            '¿Cómo funciona la carboxiterapia?'+
                            'Es un método no invasivo que consiste en la aplicación de dióxido de carbono por vía subcutánea mediante pequeñas infiltraciones.'+ 'Con esto se consigue estimular la oxidación de los ácidos grasos y reducir la celulitis y la flacidez de la piel. No suele '+
                            'presentar efectos secundarios, salvo una pequeña molestia en la zona de aplicación y un ligero enrojecimiento que dura unos '+
                            'minutos. También es posible que aparezca algún moratón tras el pinchazo '+
                            '(en función de la sensibilidad de la piel de cada persona), pero éste desaparecerá en pocos días. Una vez finalizada la sesión se'+ 'suele complementar con presoterapia o LPG para incrementar la difusión del gas a los tejidos adyacentes.'+

                            '¿Qué aplicaciones tiene?'+
                            'Las aplicaciones de esta terapia son muy diversas, pues podemos valernos de sus beneficios en tratamientos de acné, en los que se'+ 'aprovechan las propiedades antisépticas del gas para combatir la infección, bajar la inflamación de la piel del rostro, así como'+ 'regenerar la piel dañada por lo comedones (granitos e imperfecciones asociados al acné). Las estrías son otro de los problemas'+ 
                            'que podemos atacar gracias a la carboxiterapia, ya que el CO2 activa la circulación y favorece la creación de colágeno y '+
                            'elastina, atenuando la apariencia de las mismas. El dióxido de carbono también ha probado su eficacia en la reducción de '+
                            'las ojeras, principalmente si éstas son causadas por estrés o falta de sueño, y en la disminución de la caída del cabello, puesto'+ 'que mejora la circulación del cuero cabelludo, proporcionando fuerza al cabello y frenando la alopecia. Sin embargo,'+ 
                            'la carboxiterapia tiene en la reducción de la grasa localizada, la celulitis y la flacidez su mayor aplicación a nivel estético,'+ 'puesto que los resultados obtenidos son de lo más satisfactorio.'+

                            'Carboxiterapia para reducir la grasa localizada y combatir la celulitis La carboxiterapia produce en nuestro organismo un efecto'+ 'bioquímico, por lo que tiene una acción similar a la que conseguiríamos realizando ejercicio físico: reducir la grasa al '+
                            'estimular la oxidación de los ácidos grasos en el músculo. Sin embargo, este tratamiento consigue además mejorar los resultados'+ 
                            'en la grasa subcutánea, algo que el deporte por sí solo no consigue, por lo que se trata de la terapia perfecta para combinar con'+ 'unos hábitos de vida saludables y eliminar la grasa resistente a las dietas y el ejercicio.'+

                            'Número de sesiones necesarias y frecuencia'+
                            'El número de sesiones que se necesitan depende de las características y necesidades concretas de cada paciente, pero para'+ 
                            'comenzar a notar los resultados se estiman un mínimo de 15 sesiones, con una frecuencia de 2 ó 3 veces por semana. Una vez'+ 'finalizada cada sesión, no es necesario guardar ningún tipo de reposo, y podremos continuar con nuestras actividades diarias,'+ 'evitando la exposición directa al sol durante las siguientes 24-48 horas.'+
                        '</p>'+
                    '</div>'+
                '</div>';
    var pie = '';
    startModal(titulo, cuerpo, pie);
}


function viewMdImpact(){
    var titulo = 'Impact ';
    var cuerpo = '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<p>'+
                            'El IMPACT es un equipo que combina ultrasonidos de 40 KHz con corrientes de'+
                            ' electroporación destinado a tratamientos de adiposidad localizada , '+
                            'remodelación corporal, celulitis y flacidez. Tratamientos en los que se emplea: '+
                            'Cavitación Se llama cavitación a la formación de burbujas que origina la vibración '+
                            'sónica. Dichas burbujas pueden ser estables, crecer y colapsarse (implosión), '+
                            'ejerciendo con ello una acción mecánica sobre el tejido adiposo destruyendo de'+
                            'una forma no invasiva e indolora. Electroporación La electroporación consiste en'+
                            ' el uso de corrientes de media frecuencia para la abertura de microporos '+
                            'transitorios en la capa externa de la piel y en la membrana celular, lo cual facilita'+
                            ' la penetración de los activos del cosmético utilizado, e incrementando la '+
                            'efectividad del tratamiento. Es una alternativa muy eficaz e indolora a la '+
                            'mesoterapia con agujas. ¿Qué efectos produce? La acción combinada de '+
                            'ultrasonidos, lectroporación y gel activo, actúa en la remodelación corporal '+
                            'dando lugar a una importante pérdida de perímetro, actuando no sólo sobre la '+
                            'adiposidad localizada sino también sobre la celulitis y la flacidez cutánea. ¿En '+
                            'qué consiste una sesión? Un sesión con IMPACT se divide en distintas partes,'+
                            ' primero se combina la acción de los ultrasonidos con gel activo, ya sea '+
                            'anticelulítico como tensor, con corrientes de electroporación. Tras la aplicación '+
                            'de ambas técnicas (que se realiza conjuntamente ya que el cabezal de '+
                            'ultrasonidos es a su vez el electrodo de corrientes de electroporación), se realiza '+
                            'un drenaje mediante presoterapia para ayudar a eliminar la grasa que se haya '+
                            'podido liberar. ¿Cuánto dura una sesión? Depende de la zona a tratar, pero por lo '+
                            'general suele ser unos 40 minutos de ultrasonidos combinados con '+
                            'electroporación más 20 minutos de presoterapia. ¿Cuántas sesiones son '+
                            'necesarias? Dependerá de la importancia, severidad y extensión del problema, '+
                            'pero por lo general se suelen realizar unas 10 sesiones con una frecuencia de '+
                            'una sesión por semana. ¿Qué resultados se consiguen? Con IMPACT en pocas '+
                            'sesiones se reduce perímetro, se mejora de la calidad de la piel y se pierde tanto '+
                            'peso como volumen. ¿Es seguro el tratamiento? Sí, es seguro siempre y cuando '+
                            'se sigan las pautas del tratamiento y se tengan en cuenta lass contraindicaciones '+
                            'correspondientes. La combinación de ultrasonidos con electroporación es una '+
                            'forma no invasiva e indolora para reducir el espesor del panícula adiposo que no'+
                            ' representa ningún riesgo para el/la paciente siempre que se utilice siguiendo '+
                            'las pautas, indicaciones y contraindicaciones. ¿Hay que seguir algún tratamiento '+
                            'anterior o posterior? Después de una sesión con IMPACT es siempre necesario '+
                            'realizar un drenaje linfático con presoterapia, además durante todo el tratamiento '+
                            'es necesario ingeriri al menos 1.5 litros de agua diarios para ayudar a eliminar del'+
                            ' organismo la grasa destruida con el IMPACT. Como en cualquier tratamiento de '+
                            'remodelación corporal, un control de la ingesta y la toma de líquidos siempre es '+
                            'recomendable. Igualmente una vez se ha conseguido el resultado deseado, es '+
                            'importante establecer un programa de mantenimiento. ¿Cuánto tiempo ha de '+
                            'transcurrir de una sesión a otra? Lo habitual es realizar una sesión semanal, '+
                            'aunque en algunos casos se pueden realizar dos por semana, especialmente '+
                            'cuando se realizan sesiones cortas. ¿Es doloroso el tratamiento? No es doloroso. '+
                            'Lo habitual es sentir un hormigueo confortable que corresponde a la intensidad '+
                            'de las corrientes de la electroporación, aunque esta sensación varía tanto entre '+
                            'personas como entre zonas del tratamiento. Además de este hormigueo es '+
                            'normal sentir un ligero zumbido en el oído cuando esta en funcionamiento la '+
                            'cavitación. ¿Existen contraindicaciones en el tratamiento de cavitación? Como es'+
                            ' cualquier técnica hay situaciones que contraindican el uso de ultrasonidos, tales'+
                            ' como la presencia de heridas u otras patologías en la zona a tratar, prótesis '+
                            'metálicas, alteraciones de la sensibilidad cutánea, alteraciones de la coagulación '+
                            'de la sangre así como varices y/o antecedentes resientes de tromboflebitis o '+
                            'flebotrombosis. A estas contraindicaciones cabe añadirlo es las correspondientes'+
                            ' al uso de corrientes tales como portadores de marcapasos, etc.'+
                        '</p>'+
                    '</div>'+
                '</div>';
    var pie = '';
    startModal(titulo, cuerpo, pie);
}


function viewMdExilisElite(){
    var titulo = 'Exilis Elite ';
    var cuerpo = '<div class="row">'+
                    '<div class="col-md-12">'+
                        '<p>'+
                            'El IMPACT es un equipo que combina ultrasonidos de 40 KHz con corrientes de'+
                            ' electroporación destinado a tratamientos de adiposidad localizada , '+
                            'remodelación corporal, celulitis y flacidez. Tratamientos en los que se emplea: '+
                            'Cavitación Se llama cavitación a la formación de burbujas que origina la vibración '+
                            'sónica. Dichas burbujas pueden ser estables, crecer y colapsarse (implosión), '+
                            'ejerciendo con ello una acción mecánica sobre el tejido adiposo destruyendo de'+
                            'una forma no invasiva e indolora. Electroporación La electroporación consiste en'+
                            ' el uso de corrientes de media frecuencia para la abertura de microporos '+
                            'transitorios en la capa externa de la piel y en la membrana celular, lo cual facilita'+
                            ' la penetración de los activos del cosmético utilizado, e incrementando la '+
                            'efectividad del tratamiento. Es una alternativa muy eficaz e indolora a la '+
                            'mesoterapia con agujas. ¿Qué efectos produce? La acción combinada de '+
                            'ultrasonidos, lectroporación y gel activo, actúa en la remodelación corporal '+
                            'dando lugar a una importante pérdida de perímetro, actuando no sólo sobre la '+
                            'adiposidad localizada sino también sobre la celulitis y la flacidez cutánea. ¿En '+
                            'qué consiste una sesión? Un sesión con IMPACT se divide en distintas partes,'+
                            ' primero se combina la acción de los ultrasonidos con gel activo, ya sea '+
                            'anticelulítico como tensor, con corrientes de electroporación. Tras la aplicación '+
                            'de ambas técnicas (que se realiza conjuntamente ya que el cabezal de '+
                            'ultrasonidos es a su vez el electrodo de corrientes de electroporación), se realiza '+
                            'un drenaje mediante presoterapia para ayudar a eliminar la grasa que se haya '+
                            'podido liberar. ¿Cuánto dura una sesión? Depende de la zona a tratar, pero por lo '+
                            'general suele ser unos 40 minutos de ultrasonidos combinados con '+
                            'electroporación más 20 minutos de presoterapia. ¿Cuántas sesiones son '+
                            'necesarias? Dependerá de la importancia, severidad y extensión del problema, '+
                            'pero por lo general se suelen realizar unas 10 sesiones con una frecuencia de '+
                            'una sesión por semana. ¿Qué resultados se consiguen? Con IMPACT en pocas '+
                            'sesiones se reduce perímetro, se mejora de la calidad de la piel y se pierde tanto '+
                            'peso como volumen. ¿Es seguro el tratamiento? Sí, es seguro siempre y cuando '+
                            'se sigan las pautas del tratamiento y se tengan en cuenta lass contraindicaciones '+
                            'correspondientes. La combinación de ultrasonidos con electroporación es una '+
                            'forma no invasiva e indolora para reducir el espesor del panícula adiposo que no'+
                            ' representa ningún riesgo para el/la paciente siempre que se utilice siguiendo '+
                            'las pautas, indicaciones y contraindicaciones. ¿Hay que seguir algún tratamiento '+
                            'anterior o posterior? Después de una sesión con IMPACT es siempre necesario '+
                            'realizar un drenaje linfático con presoterapia, además durante todo el tratamiento '+
                            'es necesario ingeriri al menos 1.5 litros de agua diarios para ayudar a eliminar del'+
                            ' organismo la grasa destruida con el IMPACT. Como en cualquier tratamiento de '+
                            'remodelación corporal, un control de la ingesta y la toma de líquidos siempre es '+
                            'recomendable. Igualmente una vez se ha conseguido el resultado deseado, es '+
                            'importante establecer un programa de mantenimiento. ¿Cuánto tiempo ha de '+
                            'transcurrir de una sesión a otra? Lo habitual es realizar una sesión semanal, '+
                            'aunque en algunos casos se pueden realizar dos por semana, especialmente '+
                            'cuando se realizan sesiones cortas. ¿Es doloroso el tratamiento? No es doloroso. '+
                            'Lo habitual es sentir un hormigueo confortable que corresponde a la intensidad '+
                            'de las corrientes de la electroporación, aunque esta sensación varía tanto entre '+
                            'personas como entre zonas del tratamiento. Además de este hormigueo es '+
                            'normal sentir un ligero zumbido en el oído cuando esta en funcionamiento la '+
                            'cavitación. ¿Existen contraindicaciones en el tratamiento de cavitación? Como es'+
                            ' cualquier técnica hay situaciones que contraindican el uso de ultrasonidos, tales'+
                            ' como la presencia de heridas u otras patologías en la zona a tratar, prótesis '+
                            'metálicas, alteraciones de la sensibilidad cutánea, alteraciones de la coagulación '+
                            'de la sangre así como varices y/o antecedentes resientes de tromboflebitis o '+
                            'flebotrombosis. A estas contraindicaciones cabe añadirlo es las correspondientes'+
                            ' al uso de corrientes tales como portadores de marcapasos, etc.'+
                        '</p>'+
                    '</div>'+
                '</div>';
    var pie = '';
    startModal(titulo, cuerpo, pie);
}


function startModal(titulo, cuerpo, pie){
    
    $('#tituloModal').text(titulo);
    $('#cuerpoModal').html(cuerpo);
    $('#pieModal').html(pie);
    $("#modalInicio").modal();
    $("#modalInicio").css("padding-right", "0px")
    
    
}